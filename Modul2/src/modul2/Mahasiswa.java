/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul2;

/**
 *
 * @author Asus
 */
public class Mahasiswa extends Penduduk{
    private String nim;
    private String nama, tempatTanggalLahir ;
    
    public Mahasiswa(){    
    }
    public Mahasiswa(String nim, String nama, String tempatTanggalLahir) {
        this.nama = nama;
        this.nim = nim;
        this.tempatTanggalLahir = tempatTanggalLahir;

    }
    public void setNim(String nim){
        this.nim = nim;
    }
    public String getNim(){
        return nim;
    }
@Override
    public double hitungIuran() {
        double nim1 = Double.parseDouble(nim);
        return nim1 / 10000;

    }

    @Override
    public String toString() {
        double iuran = hitungIuran();
        return nim + "  |\t" + nama + "\t|   " + tempatTanggalLahir + "\t| Rp " + iuran + "\t";
    }
}
