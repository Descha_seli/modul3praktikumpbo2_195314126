/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3c;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Lat4 extends JFrame {

   JTextField wel;
    JCheckBox c, b, i;
    JTextArea t;
    JButton l, r;
    

    public Lat4() {
        setTitle("Button");
       wel = new JTextField("Welcome to Java");


        c = new JCheckBox("Centered");
        c.setBounds(230, 1, 100, 30);
        this.add(c);

        b = new JCheckBox("Bold");
        b.setBounds(230, 30, 100, 30);
        this.add(b);

        i = new JCheckBox("Italic");
        i.setBounds(230, 60, 100, 30);
        this.add(i);

        t = new JTextArea("Welcome to Java");
        this.add(t);
        t.setBounds(1, 1, 230, 85);

        l = new JButton("Left");
        l.setBounds(130, 90, 80, 20);
        this.add(l);

        r = new JButton("Right");
        r.setBounds(40, 90, 80, 20);
        this.add(r);
        this.setLayout(null);
    }

    public static void main(String[] args) {
        Lat4 empat = new Lat4();
        empat.setTitle("CheckBoxDemo");
        empat.setVisible(true);
        empat.setResizable(false);
        empat.setSize(320, 145);
        empat.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }
}