/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3c;

import java.awt.Button;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;

public class Lat2 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;


    public static void main(String[] args) {
        Lat2 dialog = new Lat2();
        dialog.setVisible(true);
    }

    public Lat2() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        Container contentPane;
        JButton YELLOW, BLUE, RED;
        setLayout(null);
        contentPane = getContentPane();

        YELLOW = new JButton("YELLOW");
        YELLOW.setBounds(25, 100, 100, 50);
        BLUE = new JButton("BLUE");
        BLUE.setBounds(175, 100, 100, 50);
        RED = new JButton("Red");
        RED.setBounds(325, 100, 100, 50);
        
        contentPane.add(YELLOW);
        contentPane.add(BLUE);
        contentPane.add(RED);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}

