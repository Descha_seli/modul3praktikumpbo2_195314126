/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3c;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Image;
import javax.swing.*;

public class Lat3 extends JDialog {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 300;

    private JLabel label_text;
    private JLabel label_image;

    public static void main(String[] args) {
        Lat3 dialog = new Lat3();
        dialog.setVisible(true);
    }

    public Lat3() {

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setTitle("Text and Icon Label");
        this.setLayout(null);

        ImageIcon gambar = new ImageIcon(getClass().getResource("Anggur.jpg"));
        label_image = new JLabel();
        label_image.setIcon(gambar);
        label_image.setBounds(200, 20, 600, 600);
        this.add(label_image);

        label_text = new JLabel("Grapes");
        label_text.setBounds(200, 20, 500, 500);
        this.add(label_text);
        
         setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }

}
