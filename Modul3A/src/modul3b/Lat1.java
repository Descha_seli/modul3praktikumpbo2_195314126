/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3b;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class Lat1 extends JFrame{
   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 200;
   private static final int FRAME_X_ORIGIN = 300;
   private static final int FRAME_Y_ORIGIN = 100;
   private static final int BUTTON_WIDTH = 80;
   private static final int BUTTON_HEIGHT = 40;
   private final JButton cancelButton;
   private final JButton okButton;
   private JTextField txtField;
   
   public static void main(String[] args) {
       Lat1 frame = new Lat1();
       frame.setVisible(true);
   }
   
 public Lat1(){
     Container contentPane = getContentPane();
     
     setSize(FRAME_WIDTH, FRAME_HEIGHT);
     setResizable(true);
     setTitle("Program Ch14AbsolutePosotioning");
     setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
     
     contentPane.setLayout( null );
     contentPane.setBackground( Color.white );
     
     okButton = new JButton("OK");
     okButton.setBounds(200, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
     contentPane.add(okButton);
     
     cancelButton = new JButton("CANCEL");
     cancelButton.setBounds(200, 150, BUTTON_WIDTH, BUTTON_HEIGHT);
     contentPane.add(cancelButton);
     
     setDefaultCloseOperation( EXIT_ON_CLOSE );
 }  

 }
