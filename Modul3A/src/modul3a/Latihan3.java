/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3a;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class Latihan3 extends JFrame {
    public Latihan3(){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel = new JPanel();
        JButton tombol = new JButton();

        tombol.setText("Ini Tombol");
        tombol.setBounds(150, 20, 100, 20);
        panel.add(tombol);
        this.add(panel);
        
        JLabel label = new JLabel();
        JTextField text = new JTextField();
        
        label.setText("ini label ");
        label.setBounds(150, 40, 110, 20);
        panel.add(label);
        this.add(panel);
        
        text.setText("ini textt ");
        text.setBounds(150, 80, 120, 20);
        panel.add(text);
        this.add(panel);
        
        JCheckBox box = new JCheckBox();
        JRadioButton radio = new JRadioButton();
        
        
        box.setText("ini box ");
        box.setBounds(150, 100, 140, 20);
        panel.add(box);
        this.add(panel);
        
        radio.setText("ini radio ");
        radio.setBounds(150, 120, 160, 20);
        panel.add(radio);
        this.add(panel);
        
    }
    public static void main(String[] args) {
        new Latihan3();
    }
}
